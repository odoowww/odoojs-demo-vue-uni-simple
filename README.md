## 联系我们

1. QQ 群 2684913
2. odoojs@outlook.com
3. 北京醉桥 负责维护 odoojs 的所有代码
4. 欢迎有志于 odoo 前后端分离项目的 个人/团队, 共同维护 odoojs
5. odoojs 团队提供 odoojs 前后端分离 解决方案付费培训服务, 请加入 qq 群, 联系群群管理员

## odoojs-demo-vue-uni-simple

1. odoojs-demo-vue-uni-simple
2. 该项目 是 odoojs 的 demo
3. 前端基于 vue3, vite, uni, uview-plus
4. 仅依赖于 odoojs-rpc

## 更新历史

1. 2023-11-17 odoojs-rpc 支持 odoo17, 并向下兼容直到 odoo13
