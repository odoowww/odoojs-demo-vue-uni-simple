import { defineConfig } from 'vite'
import uni from '@dcloudio/vite-plugin-uni'
// https://vitejs.dev/config/
export default defineConfig({
  // 打包部署后, 查找静态文件用相对路径
  base: './',
  plugins: [uni()],
  // transpileDependencies: ['uview-plus'],
  // 配置代理, 开发时解决跨域.
  server: {
    proxy: {
      // 注意 变量名 同 import.meta.env.VITE_BASE_API
      '/api': {
        target: 'http://192.168.56.103:8069', // o17
        // target: 'http://192.168.56.101:8069', //o16
        // target: 'http://192.168.56.107:8069', // o13
        // target: 'http://127.0.0.1:8069',
        // target: '/odoo',
        // target: 'http://43.143.148.83/odoo',
        // target: 'http://43.143.148.83:8069',

        changeOrigin: true,
        rewrite: path => path.replace(/^\/api/, '')
      }
    }
  }
})
