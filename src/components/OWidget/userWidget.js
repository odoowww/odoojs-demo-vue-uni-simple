import { computed } from 'vue'

export function useWidget(props, ctx = {}) {
  const { emit } = ctx

  const readonly = computed(() => {
    return !props.editable || props.node.readonly
  })

  const value2 = computed({
    get() {
      return props.record[props.node.name]
    },
    set(value) {
      // emit('update:modelValue', value)
    }
  })

  async function onChange(val) {
    emit('change', val)
  }

  return { readonly, value2, onChange }
}
