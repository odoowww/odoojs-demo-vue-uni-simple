import { rpc } from '@/odoojs/index.js'

async function test_connect() {
  const res1 = await rpc.web.webclient.version_info()

  console.log('version_info', res1)
  //   console.log(res2)
}

async function test_rpc_login() {
  const ops = await rpc.web.database.list()
  console.log('db list', ops)

  const kw = { db: 'fp2', login: 'admin', password: '123456' }
  const info = await rpc.login(kw)
  console.log('session info', info)
}

export async function test() {
  await test_connect()
  //   test_rpc_login()
}
