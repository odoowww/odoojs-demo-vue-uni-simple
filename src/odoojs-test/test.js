import { test as test0 } from './test00'

export const test = test0

const fields11 = {
  create_date: {},
  write_date: {},
  id: {},
  tracking: {},
  company_id: {
    fields: {}
  },
  location_id: {
    fields: {
      display_name: {}
    }
  },
  storage_category_id: {
    fields: {
      display_name: {}
    }
  },
  product_id: {
    fields: {
      display_name: {}
    }
  },
  product_categ_id: {
    fields: {
      display_name: {}
    }
  },
  inventory_quantity_auto_apply: {},
  reserved_quantity: {},
  currency_id: {
    fields: {}
  },
  cost_method: {},
  value: {},
  lot_properties: {}
}
