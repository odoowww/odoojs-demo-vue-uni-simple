import { createI18n } from 'vue-i18n'

import zh from './zh';
import en from './en';


const messages = {
zh,
en
};

export const i18n = new createI18n({
    locale: 'zh',
    fallbackLocale: 'en',
    messages
  });

export default{
  t:i18n.global.t,
};
