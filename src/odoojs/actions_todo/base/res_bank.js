const navigationBarTitle = {
  list: '银行',
  form({ record }) {
    return record.name
  },
  new: '银行'
}

const model_name = 'res.bank'

// const order = ''

const buttons = {
  // create: false,
  // edit: false,
  // delete: false
}

const fields = {
  active: { string: '启用' },
  name: { string: '名称', required: true, placeholder: '名称' },
  bic: { string: '代码' },
  email: { string: '邮箱' },
  phone: { string: '电话' },

  country: { string: '国家' },
  state: {
    string: '省',
    domain: ({ record }) => {
      // domain="[('country_id', '=?', country_id)]"
      const { country } = record
      return [['country_id', '=?', country]]
    }
  },
  zip: { string: '邮政编码' },
  city: { string: '市县' },
  street: { string: '街道' },
  street2: { string: '详细地址' }
}

const defaultValue = {
  name: ''
}

const rules = {
  name: {
    type: 'string',
    required: true,
    message: '请填写名称',
    trigger: ['blur', 'change']
  }
}

const kanban = {
  title({ record }) {
    return record.name
  },
  label({ record }) {
    return record.bic
  },
  value({ record }) {
    return record.email
  }
}

export default {
  model_name,
  // order,
  buttons,
  fields,
  navigationBarTitle,
  defaultValue,
  rules,
  kanban
}
