const tree_view = {
  title: '度量单位类别', // 页面 的标题, 默认取 action.name
  buttons: { create: false, edit: false, delete: false },

  fields: {
    name: {},
    reference_uom_id: {}
  },

  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return record.reference_uom_id.name
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '度量单位类别', // new 页面 的标题, 默认取 action.name
  buttons: { create: false, edit: false, delete: false },

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'uom.category',
  name: '度量单位类别',
  fields: {
    name: { string: '名称', required: true, placeholder: '名称' },
    reference_uom_id: { string: '基准单位' },
    uom_ids: {
      tree_only: 1,
      string: '度量单位',
      fields: {
        name: { string: '名称', required: true, placeholder: '名称' },
        category_id: {
          fields: {
            reference_uom_id: {}
          }
        },
        uom_type: { string: '类型' },
        ratio: { string: '转换率' }
      },
      kanban: {
        title({ record }) {
          return record.name
        },
        label({ record }) {
          return record.category_id.reference_uom_id.display_name
        },
        value({ record }) {
          const map = {
            reference: '基准单位',
            bigger: '大于基准单位',
            smaller: '小于基准单位'
          }

          const type = map[record.uom_type]

          return `${type}: ${record.ratio}`
        }
      }
    }
  },

  domain: [], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {
    default_name: 'Tag' // 设置 default
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
