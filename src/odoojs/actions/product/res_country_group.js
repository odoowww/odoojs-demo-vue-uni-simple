const tree_view = {
  title: '国家组价格表设置', // 页面 的标题, 默认取 action.name
  buttons: {
    create: false,
    //   edit: false,
    delete: false
  },

  fields: {
    name: {}
  },

  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return null
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '国家组价格表设置', // new 页面 的标题, 默认取 action.name
  buttons: {
    create: false,
    //   edit: false,
    delete: false
  },

  rules: {},

  arch: { sheet: {} }
}

export default {
  res_model: 'res.country.group',
  name: '国家组价格表设置',
  fields: {
    name: { string: '名称', readonly: true },
    pricelist_ids: {
      // tree_only: 1,
      string: '价格表',
      fields: {
        display_name: { string: '价格表名称' }
      },
      kanban: {
        title({ record }) {
          return record.display_name
        },
        label({ record }) {
          return null
        },
        value({ record }) {
          return null
        }
      }
    }
  },

  domain: [], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {
    default_name: 'Tag' // 设置 default
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
