const tree_view = {
  title: '客户价格表设置', // 页面 的标题, 默认取 action.name
  buttons: {
    create: false,
    // edit: false
    delete: false
  },

  fields: {
    name: {},
    property_product_pricelist: {}
  },

  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return record.property_product_pricelist.display_name
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '客户价格表设置', // new 页面 的标题, 默认取 action.name
  buttons: {
    create: false,
    // edit: false
    delete: false
  },

  rules: {},

  arch: { sheet: {} }
}

export default {
  res_model: 'res.partner',
  name: '客户价格表设置',
  fields: {
    name: { string: '名称', readonly: true },
    property_product_pricelist: { string: '价格表' }
  },

  domain: [
    ['is_company', '=', true],
    ['type', '=', 'contact']
  ], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {},

  views: {
    tree: tree_view,
    form: form_view
  }
}
