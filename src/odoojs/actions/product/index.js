import res_partner_org from './res_partner_org.js'
import res_country_group from './res_country_group.js'
import product_template from './product_template.js'
export default {
  res_partner_org,
  res_country_group,
  product_template
}
