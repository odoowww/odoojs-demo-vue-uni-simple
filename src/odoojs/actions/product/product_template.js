const tree_view = {
  title: '产品All', // 页面 的标题, 默认取 action.name
  buttons: {},

  fields: {
    name: {}
  },

  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return null
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '产品All', // new 页面 的标题, 默认取 action.name
  buttons: {},

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'product.template',
  name: '产品All',
  fields: {
    detailed_type: { string: '内部类型', invisible: 1 },
    sale_ok: { string: '可销售', invisible: 1 },
    purchase_ok: { string: '可采购', invisible: 1 },
    sequence: { string: '序号', invisible: 1 },
    active: { string: '激活' },
    color: { string: '颜色' },
    company_id: { string: '公司' },

    display_name: { string: '显示名称', readonly: true },

    name: { string: '名称', readonly: true },

    default_code: { string: '编码' },
    barcode: { string: '条形码' },

    description: { string: '描述' },
    description_purchase: { string: '采购说明' },
    description_sale: { string: '销售说明' },
    categ_id: { string: '类别' },
    list_price: { string: '列表价格' },
    standard_price: { string: '成本价格' },
    volume: { string: '体积' },
    volume_uom_name: { string: '体积单位' },
    weight: { string: '重量' },
    weight_uom_name: { string: '重量单位' },
    uom_id: { string: '度量单位' },
    uom_name: { string: '度量单位名称' },
    uom_po_id: { string: '采购度量单位' },

    packaging_ids: { string: '包装', invisible: 1 },
    seller_ids: { string: '供应商', invisible: 1 },
    variant_seller_ids: { string: '供应商', invisible: 1 },
    is_product_variant: { string: 'is_product_variant', invisible: 1 },
    attribute_line_ids: { string: 'attribute_line_ids', invisible: 1 },
    valid_product_template_attribute_line_ids: {
      string: 'valid_product_template_attribute_line_ids',
      invisible: 1
    },
    product_variant_ids: { string: 'product_variant_ids', invisible: 1 },
    product_variant_id: { string: 'product_variant_id', invisible: 1 }
  },

  domain: [], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {},

  views: {
    tree: tree_view,
    form: form_view
  }
}
