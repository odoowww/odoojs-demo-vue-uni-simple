import base from './base/index.js'
import uom from './uom/index.js'
import product from './product/index.js'
import analytic from './analytic/index.js'
// import om_account from './om_account/index.js'
export default {
  base,
  uom,
  product,
  analytic
  // om_account
}
