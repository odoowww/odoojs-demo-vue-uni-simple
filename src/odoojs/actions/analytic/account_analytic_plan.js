const tree_view = {
  title: '分析计划', // 页面 的标题, 默认取 action.name
  buttons: {},

  fields: {
    complete_name: {},
    account_count: {}
  },

  kanban: {
    title({ record }) {
      return record.complete_name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return `分析科目数: ${record.account_count}`
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '分析计划', // new 页面 的标题, 默认取 action.name
  buttons: {},

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'account.analytic.plan',
  name: '分析计划',
  fields: {
    company_id: { string: '公司' },

    name: { string: '名称', required: true, placeholder: '名称' },
    complete_name: { string: '全名', readonly: 1 },
    description: { string: '描述' },
    color: { string: '颜色', widget: 'color_picker', invisible: 1 },

    default_applicability: { invisible: 1 },

    parent_id: {
      string: '上级',
      invisible: 1,
      domain({ record }) {
        const { id: res_id, company_id } = record
        // [('id', '!=', id), ('company_id', 'in', [False, company_id])]
        return [
          ['id', '!=', res_id],
          ['company_id', 'in', [company_id, false]]
        ]
      }
    },
    children_ids: {
      string: '下级',
      invisible: 1,
      readonly: 1,
      tree_only: 1
    },

    account_ids: {
      string: '分析科目',
      readonly: 1,
      tree_only: 1,
      fields: {
        display_name: {}
      },

      kanban: {
        title({ record }) {
          return record.display_name
        },
        label({ record }) {
          return null
        },
        value({ record }) {
          return null
        }
      }
    },

    account_count: { string: '分析科目数', readonly: 1 },

    all_account_count: { string: '总分析科目数', invisible: 1 },

    applicability_ids: {
      string: 'Applicability',
      invisible: 1,
      readonly: 1,
      tree_only: 1,
      fields: {
        business_domain: {},
        applicability: {}
      },

      kanban: {
        title({ record }) {
          return record.display_name
        },
        label({ record }) {
          return null
        },
        value({ record }) {
          return null
        }
      }
    }
  },

  domain: [['parent_id', '=', false]], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {},

  views: {
    tree: tree_view,
    form: form_view
  }
}
