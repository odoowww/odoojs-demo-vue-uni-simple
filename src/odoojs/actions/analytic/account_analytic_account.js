const tree_view = {
  title: '分析科目', // 页面 的标题, 默认取 action.name
  buttons: {},

  fields: {
    name: {},
    balance: {}
  },

  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return `余额: ${record.balance}`
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '分析科目', // new 页面 的标题, 默认取 action.name
  buttons: {},

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'account.analytic.account',
  name: '分析科目',
  fields: {
    active: { string: '启用' },
    company_id: { string: '公司' },

    name: { string: '名称', required: true, placeholder: '名称' },
    code: { string: '代码' },
    color: { string: '颜色', widget: 'color_picker', invisible: 1 },

    plan_id: {
      string: '分析计划',
      required: true,
      domain({ record }) {
        //   [('company_id', 'in', [company_id, False])]
        const { company_id } = record
        return [['company_id', 'in', [company_id, false]]]
      }
    },

    root_plan_id: { string: '总分析计划', invisible: 1 },

    partner_id: {
      string: '联系人',
      domain({ record }) {
        //   [('company_id', 'in', [company_id, False])]
        const { company_id } = record
        return [['company_id', 'in', [company_id, false]]]
      }
    },

    line_ids: {
      string: '分析明细',
      readonly: 1,
      tree_only: 1,
      fields: {
        display_name: {}
      },

      kanban: {
        title({ record }) {
          return record.display_name
        },
        label({ record }) {
          return null
        },
        value({ record }) {
          return null
        }
      }
    },

    balance: { string: '余额', readonly: 1 },
    debit: { string: '借方', readonly: 1 },
    credit: { string: '贷方', readonly: 1 }
  },

  domain: [], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {},

  views: {
    tree: tree_view,
    form: form_view
  }
}
