const tree_view = {
  title: '分析明细', // 页面 的标题, 默认取 action.name
  buttons: {},

  fields: {
    name: {},
    date: {},
    account_id: {},
    amount: {},
    unit_amount: {}
  },

  kanban: {
    title({ record }) {
      return `${record.date} ${record.name}`
    },
    label({ record }) {
      return record.account_id.display_name
    },
    value({ record }) {
      return `金额: ${record.amount}, 数量: ${record.unit_amount}`
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '分析明细', // new 页面 的标题, 默认取 action.name
  buttons: {},

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'account.analytic.line',
  name: '分析明细',
  fields: {
    company_id: { string: '公司' },

    name: { string: '描述', required: true, placeholder: '描述' },
    date: { string: '日期', required: true },
    category: { string: '类别', invisible: true },
    plan_id: { string: '分析计划', readonly: 1 },
    account_id: {
      string: '分析科目',
      required: true,
      domain({ record }) {
        //   [('company_id', 'in', [company_id, False])]
        const { company_id } = record
        return [['company_id', 'in', [company_id, false]]]
      }
    },

    partner_id: {
      string: '参与人',
      domain({ record }) {
        //   [('company_id', 'in', [company_id, False])]
        const { company_id } = record
        return [['company_id', 'in', [company_id, false]]]
      }
    },

    user_id: { string: '管理员', readonly: 1 },

    amount: { string: '金额', required: true },
    unit_amount: { string: '数量' },

    product_uom_category_id: {
      string: '单位类别'
    },

    product_uom_id: {
      string: '单位',
      domain({ record }) {
        // [('category_id', '=', product_uom_category_id)]
        const { product_uom_category_id } = record
        return [['category_id', '=', product_uom_category_id]]
      }
    }
  },

  domain: [], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {
    default_name: 'Tag' // 设置 default
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
