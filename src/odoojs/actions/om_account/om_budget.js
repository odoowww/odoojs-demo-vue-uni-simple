import dayjs from 'dayjs'

const navigationBarTitle = {
  list: '项目管理',
  form({ record }) {
    return record.name
  },
  new: '新项目'
}

const model_name = 'crossovered.budget'

const fields = {
  name: {
    string: '项目名称',
    required: true,
    placeholder: '项目名称'
  },
  date_from: {
    string: '开始时间',
    required: true
  },
  date_to: {
    string: '结束时间',
    required: true
  },
  user_id: {
    string: '管理员',
    readonly: true
  }
}

const defaultValue = {
  name: '新项目',
  date_from: dayjs().format('YYYY-MM-DD'),
  date_to: dayjs().format('YYYY-MM-DD')
}

const rules = {
  name: {
    type: 'string',
    required: true,
    message: '请填写名称',
    trigger: ['blur', 'change']
  }
}

const kanban = {
  title({ record }) {
    return record.name
  },
  label({ record }) {
    return `${record.date_from} 到 ${record.date_to}`
  },
  value({ record }) {
    return record.user_id.display_name
  }
}

export default {
  model_name,
  fields,
  navigationBarTitle,
  defaultValue,
  rules,
  kanban
}
