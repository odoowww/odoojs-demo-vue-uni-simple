const tree_view = {
  title: '个人', // 页面 的标题, 默认取 action.name
  buttons: {},

  fields: {
    display_name: {},
    email: {}
  },

  kanban: {
    title({ record }) {
      return record.display_name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return record.email
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '个人', // new 页面 的标题, 默认取 action.name
  buttons: {},

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'res.partner',
  name: '个人',
  fields: {
    active: { string: '启用' },
    type: { string: '类型', required: true, invisible: 1 },
    is_company: { string: '是公司', required: true, invisible: 1 },
    // company_id: { string: '公司', required: true },

    parent_id: {
      string: '所有单位',
      domain: [
        ['is_company', '=', true],
        ['type', '=', 'contact']
      ]
    },
    display_name: { string: '名称', readonly: true, invisible: 1 },

    name: { string: '名称', required: true, placeholder: '名称' },

    vat: { string: '税号' },
    date: { string: '日期' },
    category_id: { string: '标签', widget: 'many2many_tags' },

    ref: { string: '代码' },
    email: { string: '邮箱' },
    phone: { string: '电话' },
    website: { string: '网站' },
    barcode: { string: '条形码' },
    comment: { string: '备注' },

    user_id: { string: '客户经理', invisible: 1 },
    bank_ids: { string: '邮箱', invisible: 1 },
    // industry_id: { string: '行业' },
    title: { string: '头衔' },

    user_ids: { string: '账号', invisible: 1 },
    employee: { string: '是员工' },
    function: { string: '职位' },
    // parent_name: { string: '上级' },
    // child_ids: { string: '下级' },

    partner_latitude: { string: '维度' },
    partner_longitude: { string: '经度' },
    country_id: { string: '国家' },
    state_id: {
      string: '省',
      domain: ({ record }) => {
        // domain="[('country_id', '=?', country_id)]"
        const { country_id } = record
        return [['country_id', '=?', country_id]]
      }
    },
    zip: { string: '邮政编码' },
    city: { string: '市县' },
    street: { string: '街道' },
    street2: { string: '详细地址' }

    //
    // company_name: { string: '国家' },
    //
  },

  domain: [
    ['is_company', '=', false],
    ['type', '=', 'contact']
  ], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {
    default_is_company: false,
    default_type: 'contact'
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
