const tree_view = {
  title: '联系人标签', // 页面 的标题, 默认取 action.name
  buttons: {},

  fields: {
    display_name: {},
    partner_ids: {}
  },

  kanban: {
    title({ record }) {
      return record.display_name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return `联系人数: ${record.partner_ids.length}`
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '联系人标签', // new 页面 的标题, 默认取 action.name
  buttons: {
    create: false,
    //   edit: false,
    delete: false
  },

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'res.partner.category',
  name: '联系人标签',
  fields: {
    display_name: { string: '全名' },
    name: { string: '名称', required: true, placeholder: '名称' },
    parent_id: { string: '上级' },
    child_ids: {
      string: '下级',
      readonly: 1,
      fields: {
        name: { string: '名称', required: true, placeholder: '名称' },
        display_name: { string: '全名' },
        partner_ids: {
          string: '联系人',
          readonly: 1,
          fields: {
            display_name: { string: '名称' }
          }
        }
      },
      kanban: {
        title({ record }) {
          return record.display_name
        },
        label({ record }) {
          return null
        },
        value({ record }) {
          return `联系人数: ${record.partner_ids.length}`
        }
      }
    },
    partner_ids: {
      string: '联系人',
      readonly: 1,
      fields: {
        display_name: { string: '名称' }
      }
    }
  },

  domain: [], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {
    default_name: 'Tag' // 设置 default
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
