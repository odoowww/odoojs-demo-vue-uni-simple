const tree_view = {
  title: '国家组', // 页面 的标题, 默认取 action.name
  buttons: {},

  fields: {
    name: {}
  },

  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return null
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '国家组', // new 页面 的标题, 默认取 action.name
  buttons: {},

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'res.country.group',
  name: '国家组',
  fields: {
    name: { string: '名称', required: true, placeholder: '名称' },
    country_ids: { string: '国家', widget: 'many2many_tags' }
  },

  domain: [], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {
    default_name: '' // 设置 default
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
