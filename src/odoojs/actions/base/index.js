import res_country_group from './res_country_group.js'
import res_country from './res_country.js'
import res_partner_industry from './res_partner_industry.js'
import res_partner_category from './res_partner_category.js'
import res_partner_title from './res_partner_title.js'
import res_bank from './res_bank.js'
import res_company from './res_company.js'
import res_users from './res_users.js'
import res_partner_org from './res_partner_org.js'
import res_partner_person from './res_partner_person.js'
import res_partner_address from './res_partner_address.js'

export default {
  res_country_group,
  res_country,
  res_partner_industry,
  res_partner_category,
  res_partner_title,
  res_bank,
  res_company,
  res_users,
  res_partner_org,
  res_partner_person,
  res_partner_address
}
