const tree_view = {
  title: '国家', // 页面 的标题, 默认取 action.name
  buttons: {
    create: false,
    //   edit: false,
    delete: false
  },

  fields: {
    code: {},
    name: {}
  },

  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return record.code
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '国家', // new 页面 的标题, 默认取 action.name
  buttons: {
    create: false,
    //   edit: false,
    delete: false
  },

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'res.country',
  name: '国家',
  fields: {
    image_url: { string: '国旗', widget: 'image_url' },
    name: { string: '名称', readonly: 1, required: true, placeholder: '名称' },
    code: { string: '编码', readonly: 1, required: true },
    state_ids: {
      // tree_only: 1,
      a1: '123123',
      string: '州省',
      context: {
        default_name: '新州',
        default_code: 'NZ'
      },
      fields: {
        name: { string: '名称', required: true, placeholder: '名称' },
        code: { string: '编码', required: true }
      },

      rules: {
        name: {
          type: 'string',
          required: true,
          message: '请填写州名称',
          trigger: ['blur', 'change']
        }
      },

      kanban: {
        title({ record }) {
          return record.name
        },
        label({ record }) {
          return null
        },
        value({ record }) {
          return record.code
        }
      }
    }
  },

  domain: [], // 默认为 空
  order: 'code', // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {
    default_name: '新国家' // 设置 default
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
