const tree_view = {
  title: '公司', // 页面 的标题, 默认取 action.name
  buttons: { create: true, edit: true, delete: false },

  fields: {
    name: {},
    email: {},
    login: {},
    login_date: {}
  },

  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return record.login
    },
    value({ record }) {
      return record.login_date
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '用户', // new 页面 的标题, 默认取 action.name
  buttons: { create: true, edit: true, delete: false },

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'res.users',
  name: '用户',
  fields: {
    active: { string: '启用' },
    partner_id: { string: '联系人', invisible: 1 },
    login: { string: '账号', required: true },
    name: { string: '名称', required: true, placeholder: '名称' },
    email: { string: '邮箱', required: true },
    login_date: { string: '上次登录时间', readonly: 1 },
    company_id: { string: '公司', required: true },
    company_ids: { string: '管理的公司', widget: 'many2many_tags' }
  },

  domain: [], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {
    default_name: '' // 设置 default
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
