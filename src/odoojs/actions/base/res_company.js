const tree_view = {
  title: '公司', // 页面 的标题, 默认取 action.name
  buttons: { create: false, edit: true, delete: false },

  fields: {
    active: {},
    logo: {},
    favicon: {},
    name: {},
    email: {}
  },

  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return record.email
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '公司', // new 页面 的标题, 默认取 action.name
  buttons: { create: false, edit: true, delete: false },

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'res.company',
  name: '公司',
  fields: {
    active: { string: '启用' },
    logo: { string: 'Logo', widget: 'image' },
    favicon: { string: '图标', widget: 'image' },
    name: { string: '名称', required: true, placeholder: '名称' },
    sequence: { string: '序号' },
    partner_id: { string: '联系人', invisible: 1 },
    parent_id: { string: '上级', readonly: 1 },
    child_ids: {
      string: '下级',
      readonly: 1,
      tree_only: 1,
      fields: { display_name: { string: '名称' } },
      kanban: {
        title({ record }) {
          return record.display_name
        },
        label({ record }) {
          return null
        },
        value({ record }) {
          return null
        }
      }
    },

    user_ids: {
      string: '用户',
      readonly: 1,
      // tree_only: 1,
      fields: { display_name: { string: '名称' } },
      kanban: {
        title({ record }) {
          return record.display_name
        },
        label({ record }) {
          return null
        },
        value({ record }) {
          return null
        }
      }
    },

    email: { string: '邮箱' },
    phone: { string: '电话' },
    vat: { string: '税号' },

    country_id: { string: '国家' },
    state_id: {
      string: '省',
      domain: ({ record }) => {
        // domain="[('country_id', '=?', country_id)]"
        const { country_id } = record
        return [['country_id', '=?', country_id]]
      }
    },
    zip: { string: '邮政编码' },
    city: { string: '市县' },
    street: { string: '街道' },
    street2: { string: '详细地址' }
  },

  domain: [], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {
    default_name: '' // 设置 default
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
