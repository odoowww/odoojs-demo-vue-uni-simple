const tree_view = {
  title: '行业', // 页面 的标题, 默认取 action.name
  buttons: { create: false, edit: false, delete: false },

  fields: {
    name: {},
    full_name: {}
  },

  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return record.full_name
    },
    value({ record }) {
      return null
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '行业', // new 页面 的标题, 默认取 action.name
  buttons: { create: false, edit: false, delete: false },

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'res.partner.industry',
  name: '行业',
  fields: {
    active: { string: '启用' },
    name: { string: '名称', required: true, placeholder: '名称' },
    full_name: { string: '全名' }
  },

  domain: [], // 默认为 空
  order: 'full_name', // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {
    default_name: '' // 设置 default
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
