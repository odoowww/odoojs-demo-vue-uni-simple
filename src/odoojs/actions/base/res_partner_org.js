const tree_view = {
  title: '组织', // 页面 的标题, 默认取 action.name
  buttons: {},

  fields: {
    name: {},
    email: {}
  },

  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return record.email
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '组织', // new 页面 的标题, 默认取 action.name
  buttons: {},

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'res.partner',
  name: '组织',
  fields: {
    active: { string: '启用' },
    type: { string: '类型', required: true, invisible: 1 },
    is_company: { string: '是公司', required: true, invisible: 1 },
    name: { string: '名称', required: true, placeholder: '名称' },
    company_id: { string: '公司', required: true },
    vat: { string: '税号' },
    date: { string: '日期' },
    category_id: { string: '标签', widget: 'many2many_tags' },

    ref: { string: '代码' },
    email: { string: '邮箱' },
    phone: { string: '电话' },
    website: { string: '网站' },
    barcode: { string: '条形码' },
    comment: { string: '备注' },

    user_id: { string: '客户经理', invisible: 1 },
    bank_ids: { string: '邮箱', invisible: 1 },
    industry_id: { string: '行业' },

    partner_latitude: { string: '维度' },
    partner_longitude: { string: '经度' },
    country_id: { string: '国家' },
    state_id: {
      string: '省',
      domain: ({ record }) => {
        // domain="[('country_id', '=?', country_id)]"
        const { country_id } = record
        return [['country_id', '=?', country_id]]
      }
    },
    zip: { string: '邮政编码' },
    city: { string: '市县' },
    street: { string: '街道' },
    street2: { string: '详细地址' }

    //
    // company_name: { string: '国家' },
    //
    // user_ids: { string: '国家' },
    // employee : { string: '职位' },
    // function: { string: '职位' },
    // parent_id: { string: '上级', invisible: 1 },
    // parent_name: { string: '上级' },
    // child_ids: { string: '下级' },
    // title: { string: '头衔' },
  },

  domain: [
    ['is_company', '=', true],
    ['type', '=', 'contact']
  ],
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {
    default_is_company: true,
    default_type: 'contact'
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
