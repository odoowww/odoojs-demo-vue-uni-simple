const tree_view = {
  title: '银行', // 页面 的标题, 默认取 action.name
  // 页面按钮, 默认 create, edit, delete 为 true
  buttons: { create: true, edit: true, delete: true },

  // table 显示时 的字段, 以及 kankan 用到的字段 必须是  action.fields 的子集
  fields: {
    name: {},
    bic: {},
    country: {},
    email: { invisible: 1 }
  },

  // kanban 显示时,
  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return record.bic
    },
    value({ record }) {
      return record.email
    }
  }

  // arch 用于定义页面结构. todo
  // arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '创建银行', // new 页面 的标题, 默认取 action.name

  // 页面按钮, 默认 create, edit, delete 为 true
  buttons: { create: true, edit: true, delete: true },

  // 编辑页面 需要的 rules, 默认 从  sheet 中 获取
  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  }

  // arch 用于定义页面结构. todo
  // arch: { sheet: {} }
}

export default {
  res_model: 'res.bank',
  name: '银行', // 默认的标题

  // 配置 字段. 配置 domain, string. 可被 view 覆盖
  fields: {
    active: { string: '启用' },
    name: { string: '名称', required: true, placeholder: '名称' },
    bic: { string: '代码' },
    email: { string: '邮箱' },
    phone: { string: '电话' },

    country: {
      string: '国家'
      //  fields: { display_name: {} }
    },
    state: {
      string: '省',
      // fields: { display_name: {} },
      domain: ({ record }) => {
        // domain="[('country_id', '=?', country_id)]"
        const { country } = record
        return [['country_id', '=?', country]]
      }
    },
    zip: { string: '邮政编码' },
    city: { string: '市县' },
    street: { string: '街道' },
    street2: { string: '详细地址' }
  },

  // list view 需要的
  domain: [], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  // form view 需要的

  context: {
    default_name: '' // 设置 default
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
