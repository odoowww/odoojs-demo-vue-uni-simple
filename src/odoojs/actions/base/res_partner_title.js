const tree_view = {
  title: '头衔', // 页面 的标题, 默认取 action.name
  buttons: {},

  fields: {
    name: {},
    shortcut: {}
  },

  kanban: {
    title({ record }) {
      return record.name
    },
    label({ record }) {
      return null
    },
    value({ record }) {
      return record.shortcut
    }
  },

  arch: { sheet: {} }
}

const form_view = {
  title({ record }) {
    return record.name
  }, // 只读或编辑页面 的标题, 默认取 action.name

  title_new: '头衔', // new 页面 的标题, 默认取 action.name
  buttons: {},

  rules: {
    name: {
      type: 'string',
      required: true,
      message: '请填写名称',
      trigger: ['blur', 'change']
    }
  },

  arch: { sheet: {} }
}

export default {
  res_model: 'res.partner.title',
  name: '头衔',
  fields: {
    name: { string: '名称', required: true, placeholder: '名称' },
    shortcut: { string: '缩写' }
  },

  domain: [], // 默认为 空
  order: undefined, // 默认为 undefined
  limit: 10, // 默认 为 10

  context: {
    default_name: 'Tag' // 设置 default
  },

  views: {
    tree: tree_view,
    form: form_view
  }
}
