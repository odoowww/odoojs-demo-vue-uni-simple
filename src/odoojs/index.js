// import { Request } from './request_uni_uview.js'
// import { OdooJSRPC } from '@/odoojs-rpc/index.js'

import { Request } from 'odoojs-rpc/dist/request_uni_uview'

import { OdooJSRPC } from 'odoojs-rpc'

// import { OdooJSRPC } from '../odoojs-rpc'

function messageError(error) {
  console.log('error', error)
  console.log('show error', error.message)
  return Promise.reject(error)
}

// 开发时, 值为 '/api', 打包部署后, 值为 '/odoo'
const baseURL = import.meta.env.VITE_BASE_API
const timeout = 50000
// const run_by_server = false // to set true if addons of web_for_odoojs installed
const run_by_server = true // to set true if addons of web_for_odoojs installed

export const rpc = new OdooJSRPC({
  Request,
  baseURL,
  timeout,
  // run_by_server,
  messageError
})

rpc.lang = 'zh_CN'

async function rpc_session_check() {
  const res = await rpc.session_check()
  if (!res) {
    uni.$u.route('pages/user/login')
  }
  return res
}

export async function session_check() {
  return rpc_session_check()
}

const api = rpc

export default api
